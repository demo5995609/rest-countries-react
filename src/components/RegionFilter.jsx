import { Box, Select } from "@chakra-ui/react";
import React from "react";

function RegionFilter({ regions, regionValue, regionInput }) {
  let options = regions.map((region, index) => {
    return (
      <option key={index} value={region}>
        {region}
      </option>
    );
  });

  return (
    <Box>
      <Select
        border={"2px solid blue"}
        value={regionInput}
        placeholder="Filter By Region-All"
        onChange={(e) => {
          regionValue(e.target.value);
        }}
      >
        {options}
      </Select>
    </Box>
  );
}

export default RegionFilter;
