import {
  Card,
  CardBody,
  Heading,
  Image,
  Stack,
  Text,
  HStack,
  VStack,
  Flex,
} from "@chakra-ui/react";
import React from "react";

function CountryCard(props) {
  let countriesCard = "";

  if (props.countriesData.length === 0) {
    return <Heading>No data Found</Heading>;
  } else {
    countriesCard = props.countriesData.map((country, index) => {
      return (
        <Card maxW="sm" key={index} mt={"15px"}>
          <CardBody>
            <Image
              src={country.flags.png}
              alt={country.flags.alt}
              borderRadius="lg"
              height={150}
              width={200}
            />
            <Stack mt="6" spacing="3">
              <Heading size="md">{country.name.common}</Heading>
              <VStack align={"flex-start"}>
                <HStack>
                  <Heading size="sm">Population: </Heading>
                  <Text>{country.population}</Text>
                </HStack>
                <HStack>
                  <Heading size="sm">Region: </Heading>
                  <Text>{country.region}</Text>
                </HStack>
                <HStack>
                  <Heading size="sm">Capital: </Heading>
                  <Text>{country.capital}</Text>
                </HStack>
              </VStack>
            </Stack>
          </CardBody>
        </Card>
      );
    });
  }

  return (
    <Flex wrap={"wrap"} flexDirection={"row"} justifyContent={"space-evenly"}>
      {countriesCard}
    </Flex>
  );
}

export default CountryCard;
