import React, { useEffect, useState } from "react";
import { SearchIcon } from "@chakra-ui/icons";
import { Box, Flex, Input } from "@chakra-ui/react";

function Search({ searchValue, inputValue }) {
  return (
    <Box>
      <Flex align={"center"} border={"2px solid blue"} borderRadius={"5px"}>
        <SearchIcon />
        <Input
          outline={"none"}
          border={"none"}
          placeholder="Search for a country"
          onChange={(e) => searchValue(e.target.value)}
          value={inputValue}
        />
      </Flex>
    </Box>
  );
}

export default Search;
