import { Box, Flex } from "@chakra-ui/react";
import React, { useState } from "react";
import Search from "./Search";
import RegionFilter from "./RegionFilter";
import SubRegionFilter from "./SubRegionFilter";

function SearchFilter({ countriesData, searchFilterData, getInputValue }) {
  return (
    <Flex
      px="1rem"
      py="2rem"
      gap={{ base: "1rem", md: "5rem" }}
      flexDirection={{ base: "column", md: "row" }}
      justify={"space-between"}
    >
      <Search getInputValue={getInputValue} />
      <RegionFilter />
      <SubRegionFilter />
    </Flex>
  );
}

export default SearchFilter;
