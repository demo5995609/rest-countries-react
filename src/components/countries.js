import React from "react";
import axios from "axios";

export const countriesData = () => {
  return axios
    .get("https://restcountries.com/v3.1/all")
    .then((res) => res.data);
};


