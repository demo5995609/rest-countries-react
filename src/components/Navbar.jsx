import React from "react";
import { Box, Button, useColorMode, useColorModeValue } from "@chakra-ui/react";
import "/src/assets/css/home.css";
import { Flex, Spacer } from "@chakra-ui/react";
import darkImg from "/src/assets/images/icons8-dark-mode-24.png";
function Navbar() {
  const { colorMode, toggleColorMode } = useColorMode();
  const bgColor = useColorModeValue("gray.50", "whiteAlpha.50");
  return (
    <Box boxShadow="md" p="6" rounded="md" bg={bgColor}>
      <Flex>
        <Box p="4">
          <a
            style={{
              fontSize: "20px",
              textDecoration: "none",
              color: "inherit",
            }}
            href="/"
          >
            <p>Where in the world?</p>
          </a>
        </Box>
        <Spacer />
        <Button p="4" className="dark-mode" onClick={toggleColorMode}>
          <img src={darkImg} alt="" />
          <span>{colorMode === "light" ? "Dark" : "Light"} Mode</span>
        </Button>
      </Flex>
    </Box>
  );
}

export default Navbar;
