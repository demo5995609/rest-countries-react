import { Box, Spinner, Text } from "@chakra-ui/react";
import React from "react";

function Loading() {
  return (
    <Box textAlign={"center"}>
      <Spinner
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="blue.500"
        size="xl"
      />
      <Text >Loading...</Text>
    </Box>
  );
}

export default Loading;
