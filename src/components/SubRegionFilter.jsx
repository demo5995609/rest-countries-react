import { Box, Select } from "@chakra-ui/react";
import React from "react";

function SubRegionFilter({ subRegions, subRegionValue, subRegionInput }) {
  let options = subRegions.map((subRegion, index) => {
    return (
      <option key={index} value={subRegion}>
        {subRegion}
      </option>
    );
  });
  return (
    <Box>
      <Select
        border={"2px solid blue"}
        placeholder="Filter By SubRegion-All"
        value={subRegionInput}
        onChange={(e) => subRegionValue(e.target.value)}
      >
        {options}
      </Select>
    </Box>
  );
}

export default SubRegionFilter;
