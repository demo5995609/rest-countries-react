import { Select } from "@chakra-ui/react";
import React from "react";

function SortCountries({ sortValueSetup }) {
  return (
    <>
      <Select
        placeholder="Sort By"
        onChange={(e) => {
          sortValueSetup(e.target.value);
        }}
        maxWidth={"200px"}
        border={"2px solid blue"}
      >
        <option key={1} value="population">
          sort by polpulation
        </option>
        <option key={2} value="area">
          sort by Area
        </option>
      </Select>
    </>
  );
}

export default SortCountries;
