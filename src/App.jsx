import { useEffect, useState } from "react";
import Navbar from "./components/Navbar";
import Search from "./components/Search";
import SortCountries from "./components/SortCountries";
import { ChakraProvider, Checkbox, Flex } from "@chakra-ui/react";

import { Container } from "@chakra-ui/react";
import CountryCard from "./components/CountryCard";
import RegionFilter from "./components/RegionFilter";
import SubRegionFilter from "./components/SubRegionFilter";
import * as restCountriesApi from "./components/countries";
import Loading from "./components/Loading";
function App() {
  const [countriesData, setCountriesData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchInput, setSearchInput] = useState("");
  const [regionInput, setRegionInput] = useState("");
  const [subRegionInput, setSubRegionInput] = useState("");
  const [landLockedInput, setLandLockedInput] = useState(false);
  const [sortValue, setSortValue] = useState("");

  useEffect(() => {
    restCountriesApi.countriesData().then((countries) => {
      setIsLoading(false);
      setCountriesData(countries);
    });
  }, []);

  function landLockedValue(data) {
    setLandLockedInput(data);
  }
  function subRegionValue(data) {
    setSubRegionInput(data);
  }
  function regionValue(data) {
    setRegionInput(data);
  }
  function searchValue(data) {
    setSearchInput(data);
  }
  function sortValueSetup(data) {
    setSortValue(data);
  }

  //Filter Base of Search Value
  let filteredCountries = countriesData.filter((country) => {
    return country.name.common
      .toLowerCase()
      .includes(searchInput.toLowerCase().trim());
  });

  //Filter Base of Region name Input
  filteredCountries =
    regionInput === ""
      ? filteredCountries
      : filteredCountries.filter((country) => {
          if (country.region === regionInput) {
            return country;
          }
        });

  // Creating subRegions object
  let subRegionObj = filteredCountries.reduce((acc, country) => {
    acc[country.subregion] = country.subregion;
    return acc;
  }, {});
  subRegionObj = Object.values(subRegionObj);

  //Filter Base of SubRegions name input
  filteredCountries =
    subRegionInput === ""
      ? filteredCountries
      : filteredCountries.filter((country) => {
          if (country.subregion === subRegionInput) {
            return country;
          }
        });

  //Filter Base of LandLocked true or false
  filteredCountries = filteredCountries.filter((country) => {
    if (country.landlocked === landLockedInput) {
      return country;
    }
  });

  //Filter Base of Sorting Calling
  sortData(sortValue);

  //Sorting Base of population and area

  function sortData(sortValue) {
    if (sortValue === "") {
      return;
    } else if (sortValue === "population") {
      filteredCountries.sort((a, b) => {
        return b.population - a.population;
      });
    } else {
      filteredCountries.sort((a, b) => {
        return b.area - a.area;
      });
    }
  }

  //Creating regions and subRegions Objects
  let regionsObj = countriesData.reduce((acc, country) => {
    acc[country.region] = country.region;
    return acc;
  }, {});
  regionsObj = Object.values(regionsObj);

  return (
    <ChakraProvider>
      <Navbar />
      <Container maxW={"1500px"}>
        <Flex
          px="1rem"
          py="2rem"
          gap={{ base: "1rem", md: "5rem" }}
          flexDirection={{ base: "column", md: "row" }}
          justify={"space-between"}
        >
          <Search searchValue={searchValue} inputValue={searchInput} />
          <SortCountries sortValueSetup={sortValueSetup} />
          <Checkbox
            border={"2px solid blue"}
            px={"10px"}
            borderRadius={"5px"}
            defaultChecked
            onChange={(e) => landLockedValue(e.target.checked)}
          >
            landlocked
          </Checkbox>
          <RegionFilter
            regions={regionsObj}
            regionValue={regionValue}
            regionInput={regionInput}
          />
          <SubRegionFilter
            subRegions={subRegionObj}
            subRegionValue={subRegionValue}
            subRegionInput={subRegionInput}
          />
        </Flex>
        {isLoading ? (
          <Loading />
        ) : (
          <CountryCard countriesData={filteredCountries} />
        )}
      </Container>
    </ChakraProvider>
  );
}

export default App;
